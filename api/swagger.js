const swaggerJSDoc = require("swagger-jsdoc");

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Portfolio-api",
      version: "1.0.0",
      description: "Portfolio API",
    },
    servers: [
      {
        url: "http://localhost:3001/",
      },
    ],
  },
  apis: ["./SRC/Models/Users/*.js", "./SRC/Routes/Users/*.js"],
};

const swaggerConfing = swaggerJSDoc(options);

module.exports = swaggerConfing;
