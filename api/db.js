const mongoose = require("mongoose");
require("dotenv").config();
const config = require("./config");

const DATABASE = config.DATABASE;

mongoose.connect(DATABASE, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
});

module.exports = mongoose;
