const User = require("../Models/Users/User");

module.exports = {
  user_findBy: async (item) => {
    let find_item;
    switch (item.findby) {
      case "code":
        find_item = await User.findOne({ code: item.code });
        return find_item;
      case "dni":
        find_item = await User.findOne({ dni: item.dni });
        return find_item;
      case "id":
        find_item = await User.findById(item.id);
      case "email":
        find_item = await User.findOne({ email: item.email });
        return find_item;
      case "user_name":
        find_item = await User.findOne({ userName: item.userName });
        return find_item;

      case "full_name":
        find_item = await User.find({
          name: item.name,
          lastName: item.lastName,
        });
        return find_item;

      case "role":
        find_item = await User.find({ role: item.role }).exec();
        return find_item;

      case "city":
        find_item = await User.find({ city: item.city });
        return find_item;

      case "tel":
        find_item = await User.find({ tel_1: item.tel_1 });
        return find_item;

      case "status":
        find_item = await User.find({ status: item.status });
        return find_item;

      case "lacation":
        let location = [];
        location.push(parseFloat(item.lat));
        location.push(parseFloat(item.log));
        find_item = await User.find({ location: location });
        return find_item;
      default:
        throw new Error();
    }
  },
};
