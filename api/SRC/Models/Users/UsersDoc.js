/**
 * @swagger
 * definitions:
 *      UserSingUp:
 *          title: "User"
 *          required:
 *              - userName
 *              - email
 *              - password
 *              - name
 *              - lastName
 *          properties:
 *              id:
 *                  type: integer
 *                  descriptions: Code user
 *              userName:
 *                  type: string
 *                  descriptions: nombre de usuario
 *              password:
 *                  type: string
 *                  descriptions: password encriptado
 *              email:
 *                  type: string
 *                  descriptions: email de usuario
 *              name:
 *                  type: string
 *                  descriptions: nombre de usuario
 *              lastName:
 *                  type: string
 *                  descriptions: apellido del usuario
 *          example:
 *              id: 1
 *              userName: Alias_usuario
 *              password: pass_usuario
 *              email: email_usuario
 *              name: nombre_usuario
 *              lastName: apellido_usuario
 *
 */

/**
 * @swagger
 * definitions:
 *      UserLogin:
 *          title: "User"
 *          descriptions: Login path / bsuca por usuario o email
 *          required:
 *              - userName
 *              - email
 *              - password
 *          properties:
 *              userName:
 *                  type: string
 *                  descriptions: nombre de usuario / Debe estar registrado
 *              password:
 *                  type: string
 *                  descriptions: password encriptado / verifica password ingresado
 *              email:
 *                  type: string
 *                  descriptions: email de usuario / verifica email existente
 *          example:
 *              userName: Alias_usuario
 *              password: pass_usuario
 *              email: email_usuario
 *
 */

/**
 * @swagger
 * definitions:
 *      UpdatedUser:
 *          title: "Updated User"
 *          descriptions: "Una vez registrado y logeado, el usuario puede actualizar sus datos de perfil el cual formará parte del CV "
 *          required:
 *              - userName
 *              - email
 *              - password
 *              - name
 *              - lastName
 *              - dni
 *              - birth
 *              - city
 *              - tel_1
 *              - tel_2
 *              - role
 *              - status
 *              - web_page
 *              - social_media
 *              - user_location
 *          properties:
 *              userName:
 *                  type: string
 *                  descriptions: nombre de usuario / Debe estar registrado
 *              password:
 *                  type: string
 *                  descriptions: password encriptado / verifica password ingresado
 *              email:
 *                  type: string
 *                  descriptions: email de usuario / verifica email existente
 *              name:
 *                  type: string
 *                  descriptions: Nombre real del usuario
 *              lastName:
 *                  type: string
 *                  descriptions: Apellido del usuario
 *              dni:
 *                  type: number
 *                  descriptions: DNI
 *              birth:
 *                  type: string
 *                  descriptions: Fecha de nacimiento
 *              city:
 *                  type: string
 *                  descriptions: Ciudad en el que vive el usuario
 *              tel_1:
 *                  type: number
 *                  descriptions: Número de telefono principal
 *              tel_2:
 *                  type: number
 *                  descriptions: Número de telefono secundario
 *              role:
 *                  type: string
 *                  descriptions: Usuario- applicant, recruiter, admin
 *              status:
 *                  type: boolean
 *                  descriptions: Estado del usuario activo/inactivo
 *              web_page:
 *                  type: string
 *                  descriptions: Página web del ususario en caso de tener pagian web personal
 *              social_media:
 *                  type: array
 *                  items:
 *                    oneOf:
 *                      - type: string
 *                  descriptions: Array con usuario de redes sociales
 *              user_location:
 *                  type: array
 *                  items:
 *                    oneOf:
 *                      - type: number
 *                  descriptions: latitud y longitud para testablecer la lacación del usuario
 *          example:
 *              userName: Alias_usuario
 *              password: pass_usuario
 *              email: email_usuario
 *              name: name_usuario
 *              lastName: lastName_usuario
 *              dni: 123456
 *              birth: 12/12/09
 *              city: city_usuario
 *              tel_1: 123456
 *              tel_2: 78910
 *              role: applicant
 *              status: true
 *              web_page: www.webusuario.com
 *              social_media: ["facebook_usuario", "instagram_usuario"]
 *              user_location: [21.00043, 34.000013]
 *
 */
