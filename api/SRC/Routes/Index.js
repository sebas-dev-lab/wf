const express = require("express");
const router = express.Router();

// Import routes
const user_routes = require("../Routes/Users/User");

// User router

router.use("/user", user_routes);

// CV router

// Jobs Router

module.exports = router;
