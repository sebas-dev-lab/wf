/**
 * @swagger
 * tags:
 *   - name: Auth
 *     description: Servicios Auth
 *   - name: Users
 *     description: Servicios para todo usuario registrado y logueado
 *   - name: Admin
 *     description: Servicios solo habilitados para el administrador - role:"admin"
 */

// !! Auth

// ?? SingUp

/**
 * @swagger
 * /user/signup:
 *   post:
 *     summary: SignUp user
 *     description:
 *     tags:
 *      - Auth
 *     consumes:
 *      - application/json
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: "body"
 *        name: "body"
 *        description: "Enviar informacion sobre el user para signup"
 *        required: true
 *        schema:
 *          $ref: "#/definitions/UserSingUp"
 *     responses:
 *       200:
 *         description: Signup successful
 *       400:
 *         description: Bad request
 *       403:
 *         description: Require token
 *       500:
 *         description: Internal server error
 */

// ?? Longin

/**
 * @swagger
 * /user/singin:
 *   post:
 *     summary: Login user
 *     description:
 *     tags:
 *      - Auth
 *     consumes:
 *      - application/json
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: "body"
 *        name: "body"
 *        description: "Datos de acceso del usuario - verifica email y alias existente junto con password correcto"
 *        required: true
 *        schema:
 *          $ref: "#/definitions/UserLogin"
 *     responses:
 *       200:
 *         description: Signup successful
 *       400:
 *         description: Bad request
 *       403:
 *         description: Require token
 *       500:
 *         description: Internal server error
 */

// !! Admin

// ?? Delete user

/**
 * @swagger
 * /user/{code}:
 *   delete:
 *      summary: Delete users
 *      description: "Acción habilitada solo para administrador - eliminar usuarios"
 *      tags:
 *       - Admin
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - in: path
 *         name: "code"
 *         description: "User code"
 *         required: true
 *         type: string
 *       - in: "header"
 *         name: "Authorization"
 *         description: "Token"
 *         required: true
 *         schema:
 *           required: true
 *           type: "string"
 *           example: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
 *      responses:
 *          200:
 *            description: Succeful
 *          400:
 *            description: Bad Request
 *          401:
 *            description: Token invalid
 *          403:
 *            description: Require Token
 *          404:
 *            description: User not found
 *          500:
 *            description: Internal server Error
 */

// !! Users

// ?? Get one user

/**
 * @swagger
 * /user/findone:
 *   get:
 *     summary: Find one users by parameters
 *     description: "Acción habilitada para todo usuario registrado y logueado - busqueda de usuarios filtradon por parametros"
 *     tags:
 *      - Users
 *     consumes:
 *      - application/json
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: "header"
 *        name: "Authorization"
 *        description: "Token"
 *        required: true
 *        schema:
 *          required: true
 *          type: "string"
 *          example: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
 *     responses:
 *       200:
 *         description: Signup successful
 *       400:
 *         description: Bad request
 *       403:
 *         description: Require token
 *       500:
 *         description: Internal server error
 */

// ?? Get all users fitered by parameters

/**
 * @swagger
 * /user:
 *   get:
 *     summary: Find all users by parameters
 *     description: "Acción habilitada para todo usuario registrado y logueado - busqueda de usuarios filtradon por parametros"
 *     tags:
 *      - Users
 *     consumes:
 *      - application/json
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: "body"
 *        name: "body"
 *        description: Get user
 *        required: true
 *      - in: "header"
 *        name: "Authorization"
 *        description: "Token"
 *        required: true
 *        schema:
 *          required: true
 *          type: "string"
 *          example: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
 *     responses:
 *       200:
 *         description: Signup successful
 *       400:
 *         description: Bad request
 *       403:
 *         description: Require token
 *       500:
 *         description: Internal server error
 */

// ?? Update User

/**
 * @swagger
 * /user:
 *   put:
 *     summary: Put User
 *     description:" Acción habilitada para todo usuario registrado y logueado - Actualizar datos del usuario: estos serán parte del perfil y CV del usuario"
 *     tags:
 *      - Users
 *     consumes:
 *      - application/json
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: "body"
 *        name: "body"
 *        description:
 *        required: true
 *        schema:
 *          $ref: "#/definitions/UpdatedUser"
 *     responses:
 *       200:
 *         description: Signup successful
 *       400:
 *         description: Bad request
 *       403:
 *         description: Require token
 *       500:
 *         description: Internal server error
 */
