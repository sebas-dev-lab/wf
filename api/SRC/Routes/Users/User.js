const express = require("express");
const route = express.Router();
const verifyAuth = require("../../Middleware/verifyAuth");
const { verify } = require("../../Middleware/auth");

// Documentation
require("./doc");

// Import Controllers
const userController = require("../../Controllers/Users/Users");
const authController = require("../../Controllers/Auth/authController");

// ?? Auth
// Sing Up
route.post(
  "/singup",
  [verifyAuth.verifyEmail, verifyAuth.verifyUserName],
  authController.postUser
);

//Login
route.post("/singin", authController.login);

// ?? Users path

// Admin
route.delete(
  "/:code",
  [verify, verifyAuth.verifyAdminRole],
  userController.delete_user
);

// All logins users
route.get("/findone", verify, userController.get_One_User);
route.get("/", verify, userController.get_users);

// User profile data update
route.put("/", verify, userController.put_user);

// Recruiter

// Applicants

module.exports = route;
