// ! Role and email verify functions
const { user_findBy } = require("../Service/fn_1");

module.exports = {
  verifyAdminRole: async (req, res, next) => {
    try {
      let { role } = req.body;
      const code = req.userCode;
      if (!role) {
        const user = await user_findBy({ findby: "code", code: code });
        if (!user) {
          return res.status(404).json({ msj: "Have not got permissions" });
        }
        role = user.role;
      }
      if (role === "admin") {
        next();
        return;
      }
      return res.status(401).json({ msj: "User have not permission" });
    } catch (e) {
      console.error(e);
    }
  },
  verifyRecruiterRole: () => {
    try {
      const { role } = req.body;
      if (!role)
        return res.status(404).json({ msj: "User have not got a role" });
      if (role === "recruiter") {
        next();
        return;
      }
      return res.status(401).json({ msj: "User have not permission" });
    } catch (e) {
      console.error(e);
    }
  },
  verifyApplicantRole: () => {
    try {
      const { role } = req.body;
      if (!role)
        return res.status(404).json({ msj: "User have not got a role" });
      if (role === "applicant") {
        next();
        return;
      }
      return res.status(401).json({ msj: "User have not permission" });
    } catch (e) {
      console.error(e);
    }
  },
  verifyEmail: async (req, res, next) => {
    try {
      const { email } = req.body;
      if (!email) return res.status(404).json({ msj: "Email is required" });
      const user = await user_findBy({ findby: "email", email: email });
      if (user) {
        return res.status(404).json({ msj: "The email already exist" });
      }
      next();
    } catch (e) {
      console.error(e);
    }
  },
  verifyUserName: async (req, res, next) => {
    try {
      const { userName } = req.body;
      if (!userName) {
        return res.status(404).json({ msj: "User Name is required" });
      }
      const user = await user_findBy({
        findby: "user_name",
        userName: userName,
      });
      console.log(user);
      if (user) return res.status(400).json({ msj: "User Name already exist" });
      next();
    } catch (e) {
      console.error(e);
    }
  },
};
