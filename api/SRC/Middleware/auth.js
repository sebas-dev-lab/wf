const jwt = require("jsonwebtoken");
const config = require("../../config");
const { user_findBy } = require("../Service/fn_1");

// Veirify Token
exports.verify = async (req, res, next) => {
  try {
    const token = req.headers["x-access-token"];
    if (!token) {
      return res.status(401).json({
        auth: false,
        msj: "Have not got token",
      });
    }
    const decoded = jwt.verify(token, config.secret);
    req.userCode = decoded.id;
    const user = await user_findBy({ findby: "code", code: req.userCode });
    if (!user) return res.status(404).json({ msj: "User could not be found" });

    next();
  } catch (e) {
    console.error(e);
  }
};
