const User = require("../../Models/Users/User");
const { user_findBy } = require("../../Service/fn_1");

// Find one user by: code, email, dni, userName
// All users will can find-Only Register ando login users
exports.get_One_User = async (req, res) => {
  try {
    const find_with = req.body;
    // Remember: {findby, code, userName,dni, email}=req.body
    const user = await user_findBy(find_with);
    if (!user) {
      return res.status(404).json({ msj: "User could not be found" });
    }
    return res.status(200).json({ msj: "ok", user: user });
  } catch (e) {
    console.error(e);
  }
};

// Get list of users by: Name and Last Name, role, location, city, tel_1, status
// All users will can find-Only Register ando login users
exports.get_users = async (req, res) => {
  try {
    const find_with = req.body;
    // Remember: {findby, name, lastName, role, lat, log, city, tel_1, status}=req.body
    const users = await user_findBy(find_with);
    if (users.length === 0) {
      return res.status(400).json({ msj: "Users could not be found" });
    }
    return res.status(200).json({ msj: "ok", users: users });
  } catch (e) {
    console.error(e);
  }
};

// Delete user by code - Only Admin
exports.delete_user = async (req, res) => {
  try {
    const { code } = req.params;
    const user = await User.findOne({ code: code });
    if (!user) {
      return res.status(400).json({ msj: "User is not in db" });
    }
    await User.deleteOne({ code: code });
    let item = (await User.find({ code: code })).forEach((item) =>
      item.code === code ? item : null
    );
    if (item) {
      return res.status(404).json({ msj: "Element could not be deleted" });
    }
    return res.status(200).json({ msj: "ok" });
  } catch (e) {
    console.error(e);
  }
};

// Put user by code - All users can modify your profile only
exports.put_user = async (req, res) => {
  try {
    const code = req.userCode;
    console.log(code);
    let {
      userName,
      password,
      name,
      lastName,
      dni,
      birth,
      city,
      email,
      tel_1,
      tel_2,
      role,
      web_page,
      facebook,
      twiter,
      instagram,
      linkedin,
      github,
      status,
      lat,
      log,
    } = req.body;
    if (
      !userName ||
      !password ||
      !code ||
      !name ||
      !lastName ||
      !dni ||
      !email ||
      !tel_1 ||
      !role ||
      !city
    ) {
      return res.status(404).json({ msj: "Datas are required" });
    }
    const user = await user_findBy({ findby: "code", code: code });
    if (!user) {
      return res.status(404).json({ msj: "User could not be found" });
    }

    password = await user.encrypt(password);

    await User.findOneAndUpdate(
      { code: code },
      {
        userName,
        password,
        name,
        lastName,
        dni,
        birth,
        city,
        email,
        tel_1,
        tel_2,
        role,
        web_page,
        facebook,
        twiter,
        instagram,
        linkedin,
        github,
        status,
        lat,
        log,
      }
    );
    return res.status(200).json({ msj: "ok", user: user });
  } catch (e) {
    console.error(e);
  }
};
