const { user_findBy } = require("../../Service/fn_1");
const { createToken } = require("../../Service/auth");
const User = require("../../Models/Users/User");

//  Create user - singUp
exports.postUser = async (req, res) => {
  try {
    const { userName, password, name, lastName, email, role } = req.body;

    if (!userName && !password && !name && !lastName && !email && !role) {
      return res.status(404).json({ msj: "Parameters is required" });
    }

    const user = new User({
      userName,
      password,
      name,
      lastName,
      email,
      role,
    });
    if (!user) {
      return res.status(404).json({ msj: "User could not be created" });
    }
    user.password = await user.encrypt(password);
    await user.save();

    const token = createToken(user);
    return res
      .status(200)
      .json({ msj: "ok", auth: true, token: token, user: user });
  } catch (e) {
    console.error(e);
  }
};

// SingIn Users
exports.login = async (req, res) => {
  // find_with: findby/email/user/password
  const { findby, email, userName, password } = req.body;
  const user = await user_findBy({ findby, email, userName });
  if (!user) {
    return res.status(404).json({ msj: "User Could not be found" });
  }
  const isValid = await user.validatePassword(password);
  if (!isValid) {
    return res
      .status(401)
      .json({ auth: false, token: null, msj: "Password incorrect" });
  }
  const token = createToken(user);

  return res.status(200).json({ auth: true, token, user: user, msj: "ok" });
};
