module.exports = {
  DATABASE: process.env.DATABASE,
  PORT: process.env.PORT,
  secret: process.env.SECRET,
};
