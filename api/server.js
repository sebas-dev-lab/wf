const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const multer = require("multer");
const path = require("path");
const uuid = require("uuid");
require("dotenv").config();
const config = require("./config");
const swaggerUI = require("swagger-ui-express");
const swaggerConfing = require("./swagger");

// Import Routes
const routes = require("./SRC/Routes/Index");

// Server
const app = express();
app.set("port", config.PORT || 3001);

// Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const storage = multer.diskStorage({
  destination: path.join(__dirname, "public/img/uploads"),
  filename: (req, file, cb, filename) => {
    cb(null, uuid() + Date.now() + path.extname(file.originalname));
  },
});
app.use(multer({ storage: storage }).array("images"));

// Routes

app.use("/", routes);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerConfing));

module.exports = app;
